# HTML / CSS Assessment
### _**Author**: Luis García Castro_
### _**Date**: 20/Jun/2019_

# Environment Preparation
1. npm install

1. npm start

1. Go to: http://localhost:8080

# Showcase

* Desktop:

![Chrome](./documentation/chrome_desktop.png)

![Firefox](./documentation/firefox_desktop.png)

![Safari](./documentation/safari_desktop.png)

![Edge](./documentation/edge_desktop.png)

![IE 11](./documentation/ie11_desktop.png)

* Tablet:

![Safari Tablet](./documentation/safari_ios_tablet.png)

* Mobile

![Chrome Android 1](./documentation/chrome_android.png)
![Chrome Android 2](./documentation/chrome_android_2.png)

![Safari iOS](./documentation/safari_ios.png)
